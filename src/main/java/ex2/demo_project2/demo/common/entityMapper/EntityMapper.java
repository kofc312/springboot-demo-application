package ex2.demo_project2.demo.common.entityMapper;

import java.util.List;

public interface EntityMapper<d, Et> {
    Et toEntity(d dto);
    d toDto(Et entity);
    List<Et> toEntity(List<d> voList);
    List<d> toDto(List<Et> entityList);
}
