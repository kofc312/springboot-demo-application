package ex2.demo_project2.demo.domain.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class dto_TblContentFreeBoard {
    public String WRITE_DATE;
    public String ID;
    public String SUBJECT;
    public String CONTENT;
    public boolean IS_VALID;
}
