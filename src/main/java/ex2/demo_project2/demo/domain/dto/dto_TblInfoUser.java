package ex2.demo_project2.demo.domain.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class dto_TblInfoUser {
    public String ID;
    public String PW;
    public String AU_ID;
    public String NAME;
    public int GENDER;
    public boolean IS_VALID;
}
