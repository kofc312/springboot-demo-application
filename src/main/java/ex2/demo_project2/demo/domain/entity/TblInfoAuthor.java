package ex2.demo_project2.demo.domain.entity;

import lombok.AccessLevel;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Entity(name = "TBL_INFO_AUTHOR")
@Table(name = "TBL_INFO_AUTHOR")
public class TblInfoAuthor {
    @Id
    @Column(name = "AU_ID")
    private String AU_ID;

    @Builder
    public TblInfoAuthor (String AU_ID) {
        this.AU_ID = AU_ID;
    }
}
