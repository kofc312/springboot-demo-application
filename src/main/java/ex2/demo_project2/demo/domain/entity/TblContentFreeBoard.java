package ex2.demo_project2.demo.domain.entity;

import lombok.AccessLevel;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import javax.persistence.*;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Entity(name = "TBL_CONTENT_FREEBOARD")
@Table(name = "TBL_CONTENT_FREEBOARD")
public class TblContentFreeBoard {
    @Id
    @Column(name = "WRITE_DATE")
    private String WRITE_DATE;

    @Column(name = "ID")
    private String ID;

    @Column(name = "SUBJECT")
    private String SUBJECT;

    @Column(name = "CONTENT")
    private String CONTENT;

    @Column(name = "IS_VALID")
    private boolean IS_VALID;

    @ManyToOne
    @JoinColumn(
            name = "ID",
            referencedColumnName = "ID",
            insertable = false,
            updatable = false
    )
    private TblInfoUser tblInfoUser;

    @Builder
    public TblContentFreeBoard (String WRITE_DATE, String ID, String SUBJECT, String CONTENT, boolean IS_VALID) {
        this.WRITE_DATE = WRITE_DATE;
        this.ID = ID;
        this.SUBJECT = SUBJECT;
        this.CONTENT = CONTENT;
        this.IS_VALID = IS_VALID;
    }
}
