package ex2.demo_project2.demo.domain.entity;

import lombok.AccessLevel;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Entity(name = "TBL_INFO_USER")
@Table(name = "TBL_INFO_USER")
public class TblInfoUser {
    @Id
    @Column(name="ID")
    private String ID;

    @Column(name="PW")
    private String PW;

    @Column(name = "AU_ID")
    private String AU_ID;

    @Column(name="NAME")
    private String NAME;

    @Column(name="GENDER")
    private int GENDER;

    @Column(name="IS_VALID")
    private boolean IS_VALID;

    @ManyToOne
    @JoinColumn(
            name = "AU_ID",
            referencedColumnName = "AU_ID",
            insertable = false,
            updatable = false
    )
    private TblInfoAuthor tblInfoAuthor;

    @Builder
    public TblInfoUser (String ID, String PW, String AU_ID, String NAME, int GENDER, boolean IS_VALID) {
        this.ID = ID;
        this.PW = PW;
        this.AU_ID = AU_ID;
        this.NAME = NAME;
        this.GENDER = GENDER;
        this.IS_VALID = IS_VALID;
    }
}