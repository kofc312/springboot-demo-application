package ex2.demo_project2.demo.domain.mapper;

import org.mapstruct.Mapper;

import ex2.demo_project2.demo.common.entityMapper.EntityMapper;
import ex2.demo_project2.demo.domain.dto.dto_TblInfoAuthor;
import ex2.demo_project2.demo.domain.entity.TblInfoAuthor;

@Mapper(componentModel = "spring")
public interface mp_TblInfoAuthor extends EntityMapper<dto_TblInfoAuthor, TblInfoAuthor> {
}
