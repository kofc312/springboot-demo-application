package ex2.demo_project2.demo.domain.mapper;

import org.mapstruct.Mapper;

import ex2.demo_project2.demo.common.entityMapper.EntityMapper;
import ex2.demo_project2.demo.domain.dto.dto_LogTblInfoUser;
import ex2.demo_project2.demo.domain.entity.LogTblInfoUser;

@Mapper(componentModel = "spring")
public interface mp_LogTblInfoUser extends EntityMapper<dto_LogTblInfoUser, LogTblInfoUser> {
}
