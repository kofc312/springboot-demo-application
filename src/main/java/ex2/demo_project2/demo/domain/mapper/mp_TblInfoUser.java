package ex2.demo_project2.demo.domain.mapper;

import org.mapstruct.Mapper;

import ex2.demo_project2.demo.common.entityMapper.EntityMapper;
import ex2.demo_project2.demo.domain.dto.dto_TblInfoUser;
import ex2.demo_project2.demo.domain.entity.TblInfoUser;

@Mapper(componentModel = "spring")
public interface mp_TblInfoUser extends EntityMapper<dto_TblInfoUser, TblInfoUser> {
}
