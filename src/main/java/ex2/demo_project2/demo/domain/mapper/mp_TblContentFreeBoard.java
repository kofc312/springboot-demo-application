package ex2.demo_project2.demo.domain.mapper;

import org.mapstruct.Mapper;

import ex2.demo_project2.demo.common.entityMapper.EntityMapper;
import ex2.demo_project2.demo.domain.dto.dto_TblContentFreeBoard;
import ex2.demo_project2.demo.domain.entity.TblContentFreeBoard;

@Mapper(componentModel = "spring")
public interface mp_TblContentFreeBoard extends EntityMapper<dto_TblContentFreeBoard, TblContentFreeBoard> {
}
