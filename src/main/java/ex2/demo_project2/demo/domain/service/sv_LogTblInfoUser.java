package ex2.demo_project2.demo.domain.service;

import ex2.demo_project2.demo.domain.vo.voOut_ReturnLogTblInfoUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import ex2.demo_project2.demo.domain.dto.dto_LogTblInfoUser;
import ex2.demo_project2.demo.domain.entity.LogTblInfoUser;
import ex2.demo_project2.demo.domain.mapper.mp_LogTblInfoUser;
import ex2.demo_project2.demo.domain.repo.queryDsl.rs_LogTblInfoUser;
import ex2.demo_project2.demo.domain.repo.repo_LogTblInfoUser;
import ex2.demo_project2.demo.domain.repo.repo_TblInfoUser;
import ex2.demo_project2.demo.domain.vo.voIn_AddNewUser;

@Service
@Transactional
public class sv_LogTblInfoUser {
    private DateFormat stringToDateTime6 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSSz");

    @Autowired
    repo_LogTblInfoUser repo_logTblInfoUser;

    @Autowired
    repo_TblInfoUser repo_tblInfoUser;

    @Autowired
    rs_LogTblInfoUser rs_logTblInfoUser;

    mp_LogTblInfoUser mp_logTblInfoUser;

    public sv_LogTblInfoUser(mp_LogTblInfoUser mp) {
        this.mp_logTblInfoUser = mp;
    }

    public String writeLogAtTblUserInfo(voIn_AddNewUser user) {
        String result = "Failed";

        if (!repo_tblInfoUser.findById(user.ID).isPresent()) {
            result = "Failed - Not Found This User";
        }

        else {
            if (user.PW.length() > 20 && user.PW.length() < 4) {
                result = "Failed - 4자 이상 20자 미만 입력 가능";
            }

            else if (user.NAME.length() > 10 && user.NAME.length() < 2) {
                result = "Failed - 2자 이상 10자 미만 입력 가능";
            }

            else if (user.GENDER < 1) {
                result = "Failed - 올바르지 않은 성별 (1: 남성, 2: 여성)";
            }

            else if (user.GENDER > 2) {
                result = "Failed - 올바르지 않은 성별 (1: 남성, 2: 여성)";
            }

            else {
                try {
                    dto_LogTblInfoUser convertToDto = new dto_LogTblInfoUser();

                    convertToDto.WRITE_DATE = stringToDateTime6.format(Calendar.getInstance().getTime());
                    convertToDto.ID = user.ID;
                    convertToDto.PW = user.PW;
                    convertToDto.AU_ID = user.AU_ID;
                    convertToDto.NAME = user.NAME;
                    convertToDto.GENDER = user.GENDER;
                    convertToDto.IS_VALID = true;

                    LogTblInfoUser passEntity = mp_logTblInfoUser.toEntity(convertToDto);
                    LogTblInfoUser entity_saveResult = repo_logTblInfoUser.saveAndFlush(passEntity);

                    if (entity_saveResult != null) {
                        result = "Success";
                    }

                    else {
                        result = "Failed";
                    }
                }

                catch (Exception ex) {
                    System.out.println(ex.toString());
                    result = "Error";
                }
            }
        }

        return result;
    }

    public List<voOut_ReturnLogTblInfoUser> getAllLogUserInfo() {
        List<voOut_ReturnLogTblInfoUser> result = new ArrayList<>();

        try {
            List<LogTblInfoUser> getLogUserEntityList = rs_logTblInfoUser.findAll();
            List<dto_LogTblInfoUser> toDto_userList = mp_logTblInfoUser.toDto(getLogUserEntityList);

            for (int i = 0; i < toDto_userList.size(); i++) {
                voOut_ReturnLogTblInfoUser item = new voOut_ReturnLogTblInfoUser();

                item.WRITE_DATE = toDto_userList.get(i).WRITE_DATE;
                item.ID = toDto_userList.get(i).ID;
                item.AU_ID = toDto_userList.get(i).AU_ID;
                item.NAME = toDto_userList.get(i).NAME;
                item.GENDER = toDto_userList.get(i).GENDER;
                item.IS_VALID = toDto_userList.get(i).IS_VALID;

                item.Status = "Success";

                result.add(item);
            }
        }

        catch (Exception ex) {
            voOut_ReturnLogTblInfoUser item = new voOut_ReturnLogTblInfoUser();

            item.WRITE_DATE = null;
            item.ID = null;
            item.AU_ID = null;
            item.NAME = null;
            item.GENDER = -1;
            item.IS_VALID = false;

            item.Status = "Error";

            result.add(item);
        }

        return result;
    }
}
