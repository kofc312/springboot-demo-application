package ex2.demo_project2.demo.domain.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import java.util.ArrayList;
import java.util.List;

import ex2.demo_project2.demo.domain.dto.dto_TblInfoUser;
import ex2.demo_project2.demo.domain.entity.TblInfoUser;
import ex2.demo_project2.demo.domain.mapper.mp_TblInfoUser;
import ex2.demo_project2.demo.domain.repo.queryDsl.rs_TblInfoUser;
import ex2.demo_project2.demo.domain.repo.repo_TblInfoUser;
import ex2.demo_project2.demo.domain.vo.voIn_AddNewUser;
import ex2.demo_project2.demo.domain.vo.voOut_ReturnUserInfoWithStatusString;

@Service
@Transactional
public class sv_TblInfoUser {
    // 주석 추가했지롱요

    @Autowired
    repo_TblInfoUser repo_tblInfoUser;

    @Autowired
    rs_TblInfoUser rs_tblInfoUser;

    @Autowired
    sv_LogTblInfoUser sv_logTblInfoUser;

    mp_TblInfoUser mp_tblInfoUser;

    public sv_TblInfoUser(mp_TblInfoUser mp) {
        this.mp_tblInfoUser = mp;
    }

    public List<voOut_ReturnUserInfoWithStatusString> getAllUserInfo() {
        List<voOut_ReturnUserInfoWithStatusString> result = new ArrayList<>();

        try {
            List<TblInfoUser> getUserEntityList = rs_tblInfoUser.findAll();
            List<dto_TblInfoUser> toDto_userList = mp_tblInfoUser.toDto(getUserEntityList);

            for (int i = 0; i < toDto_userList.size(); i++) {
                voOut_ReturnUserInfoWithStatusString item = new voOut_ReturnUserInfoWithStatusString();

                item.ID = toDto_userList.get(i).ID;
                item.PW = toDto_userList.get(i).PW;
                item.AU_ID = toDto_userList.get(i).AU_ID;
                item.NAME = toDto_userList.get(i).NAME;
                item.GENDER = toDto_userList.get(i).GENDER;
                item.IS_VALID = toDto_userList.get(i).IS_VALID;

                item.Status = "Success";

                result.add(item);
            }
        }

        catch (Exception ex) {
            voOut_ReturnUserInfoWithStatusString item = new voOut_ReturnUserInfoWithStatusString();

            item.ID = null;
            item.PW = null;
            item.AU_ID = null;
            item.NAME = null;
            item.GENDER = -1;
            item.IS_VALID = false;

            item.Status = "Error";

            result.add(item);
        }

        return result;
    }

    public voOut_ReturnUserInfoWithStatusString searchUserIdAndPw(String ID, String PW) {
        voOut_ReturnUserInfoWithStatusString result = new voOut_ReturnUserInfoWithStatusString();

        try {
            dto_TblInfoUser dto = mp_tblInfoUser.toDto(rs_tblInfoUser.findByIdAndPw(ID, PW).get(0));

            result.ID = dto.ID;
            result.PW = dto.PW;
            result.AU_ID = dto.AU_ID;
            result.NAME = dto.NAME;
            result.GENDER = dto.GENDER;
            result.IS_VALID = dto.IS_VALID;
            result.Status = "Success";
        }

        catch (Exception ex) {
            result.ID = null;
            result.PW = null;
            result.AU_ID = null;
            result.NAME = null;
            result.GENDER = -1;
            result.IS_VALID = false;
            result.Status = "Error";
        }

        return result;
    }

    public String addNewUser(voIn_AddNewUser user) {
        String result = "Failed";

        if (repo_tblInfoUser.findById(user.ID).isPresent()) {
            result = "Failed - 중복된 아이디";
        }

        else if (user.ID.length() > 20) {
            result = "Failed - 20자 미만 입력 가능";
        }

        else if (user.PW.length() > 20) {
            result = "Failed - 20자 미만 입력 가능";
        }

        else if (user.NAME.length() > 10) {
            result = "Failed - 10자 미만 입력 가능";
        }

        else if (user.GENDER < 1) {
            result = "Failed - 올바르지 않은 성별 (1: 남성, 2: 여성)";
        }

        else if (user.GENDER > 2) {
            result = "Failed - 올바르지 않은 성별 (1: 남성, 2: 여성)";
        }

        else {
            try {
                dto_TblInfoUser convertToDto = new dto_TblInfoUser();
                convertToDto.ID = user.ID;
                convertToDto.PW = user.PW;
                convertToDto.AU_ID = "USER";
                convertToDto.NAME = user.NAME;
                convertToDto.GENDER = user.GENDER;
                convertToDto.IS_VALID = true;

                TblInfoUser passEntity = mp_tblInfoUser.toEntity(convertToDto);
                TblInfoUser entity_saveResult = repo_tblInfoUser.saveAndFlush(passEntity);

                if (entity_saveResult != null) {
                    if (sv_logTblInfoUser.writeLogAtTblUserInfo(user) == "Success") {
                        result = "Success";
                    }

                    else {
                        result = "Failed - Log Failed";
                    }
                }

                else {
                    result = "Failed";
                }
            }

            catch (Exception ex) {
                System.out.println(ex.toString());
                result = "Error";
            }
        }

        return result;
    }

    public String updateUserInfo(voIn_AddNewUser user) {
        String result = "Failed";

        if (!repo_tblInfoUser.findById(user.ID).isPresent()) {
            result = "Failed - Not Found This User";
        }

        else if (user.NAME.length() > 10 && user.NAME.length() < 2) {
            result = "Failed - 2자 이상 10자 미만 입력 가능";
        }

        else if (user.GENDER < 1) {
            result = "Failed - 올바르지 않은 성별 (1: 남성, 2: 여성)";
        }

        else if (user.GENDER > 2) {
            result = "Failed - 올바르지 않은 성별 (1: 남성, 2: 여성)";
        }

        else {
            dto_TblInfoUser convertToDto = new dto_TblInfoUser();
            convertToDto.ID = user.ID;
            convertToDto.PW = user.PW;
            convertToDto.AU_ID = user.AU_ID;
            convertToDto.NAME = user.NAME;
            convertToDto.GENDER = user.GENDER;
            convertToDto.IS_VALID = true;

            TblInfoUser passEntity = mp_tblInfoUser.toEntity(convertToDto);
            TblInfoUser entity_saveResult = repo_tblInfoUser.saveAndFlush(passEntity);

            if (entity_saveResult != null) {
                if (sv_logTblInfoUser.writeLogAtTblUserInfo(user) == "Success") {
                    result = "Success";
                }

                else {
                    result = "WARN - 로그 기록 실패";
                }
            }

            else {
                result = "Failed";
            }
        }

        return result;
    }


}
