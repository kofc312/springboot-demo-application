package ex2.demo_project2.demo.domain.service;

import com.querydsl.core.Tuple;
import ex2.demo_project2.demo.domain.dto.dto_TblContentFreeBoard;
import ex2.demo_project2.demo.domain.entity.TblContentFreeBoard;
import ex2.demo_project2.demo.domain.entity.TblInfoUser;
import ex2.demo_project2.demo.domain.mapper.mp_TblContentFreeBoard;
import ex2.demo_project2.demo.domain.repo.queryDsl.rs_TblContentFreeBoard;
import ex2.demo_project2.demo.domain.repo.queryDsl.rs_TblInfoUser;
import ex2.demo_project2.demo.domain.repo.repo_TblContentFreeBoard;
import ex2.demo_project2.demo.domain.repo.repo_TblInfoUser;
import ex2.demo_project2.demo.domain.vo.voIn_AddNewFreeBoardContent;
import ex2.demo_project2.demo.domain.vo.voOut_ReturnFreeBoardContentJoinAuthor;
import ex2.demo_project2.demo.domain.vo.voOut_ReturnFreeBoardWriteResultWithStatusString;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

@Service
@Transactional
public class sv_TblContentFreeBoard {
    private DateFormat stringToDateTime6 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSSz");

    @Autowired
    repo_TblContentFreeBoard repo_tblContentFreeBoard;

    @Autowired
    rs_TblInfoUser rs_tblInfoUser;

    @Autowired
    rs_TblContentFreeBoard rs_tblContentFreeBoard;

    mp_TblContentFreeBoard mp_tblContentFreeBoard;

    public sv_TblContentFreeBoard(mp_TblContentFreeBoard mp) {
        this.mp_tblContentFreeBoard = mp;
    }

    public voOut_ReturnFreeBoardWriteResultWithStatusString add_newFreeBoardContent(voIn_AddNewFreeBoardContent content) {
        voOut_ReturnFreeBoardWriteResultWithStatusString result = new voOut_ReturnFreeBoardWriteResultWithStatusString();

        try {
            String findID = rs_tblInfoUser.findById(content.ID);

            if (findID == null) {
                result.WRITE_DATE = null;
                result.ID = null;
                result.SUBJECT = null;
                result.Status = "Failed";
            }

            else if (findID.equals(content.ID)) {
                dto_TblContentFreeBoard convertToDto = new dto_TblContentFreeBoard();
                convertToDto.WRITE_DATE = stringToDateTime6.format(Calendar.getInstance().getTime());
                convertToDto.ID = content.ID;
                convertToDto.SUBJECT = content.SUBJECT;
                convertToDto.CONTENT = content.CONTENT;
                convertToDto.IS_VALID = true;

                TblContentFreeBoard passEntity = mp_tblContentFreeBoard.toEntity(convertToDto);
                TblContentFreeBoard entity_saveResult = repo_tblContentFreeBoard.saveAndFlush(passEntity);

                if (entity_saveResult != null){
                    result.WRITE_DATE = convertToDto.WRITE_DATE;
                    result.ID = content.ID;
                    result.SUBJECT = content.SUBJECT;
                    result.Status = "Success";
                }

                else {
                    result.WRITE_DATE = null;
                    result.ID = null;
                    result.SUBJECT = null;
                    result.Status = "Failed";
                }
            }

            else {
                result.WRITE_DATE = null;
                result.ID = null;
                result.SUBJECT = null;
                result.Status = "Failed - Not Found This User";
            }
        }

        catch (Exception ex) {
            System.out.println(ex.toString());

            result.WRITE_DATE = null;
            result.ID = null;
            result.SUBJECT = null;
            result.Status = "Failed - Not Found This User";
        }

        return result;
    }

    public List<voOut_ReturnFreeBoardContentJoinAuthor> get_allContentJoinAuthor() {
        List<voOut_ReturnFreeBoardContentJoinAuthor> result = new ArrayList<>();

        List<Tuple> getAllContent = rs_tblContentFreeBoard.getAll_joinWithAuthor();

        for (int i = 0; i < getAllContent.size(); i++) {
            voOut_ReturnFreeBoardContentJoinAuthor item = new voOut_ReturnFreeBoardContentJoinAuthor();

            //item.WRITE_DATE = getAllContent.get(i).get();
        }

        return result;
    }
}
