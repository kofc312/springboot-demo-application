package ex2.demo_project2.demo.domain.vo;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class voIn_AddNewFreeBoardContent {
    @JsonProperty("ID")
    public String ID;

    @JsonProperty("SUBJECT")
    public String SUBJECT;

    @JsonProperty("CONTENT")
    public String CONTENT;
}
