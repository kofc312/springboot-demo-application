package ex2.demo_project2.demo.domain.vo;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class voOut_ReturnFreeBoardContentJoinAuthor {
    @JsonProperty("WRITE_DATE")
    public String WRITE_DATE;

    @JsonProperty("ID")
    public String ID;

    @JsonProperty("NAME")
    public String NAME;

    @JsonProperty("AU_ID")
    public String AU_ID;

    @JsonProperty("Status")
    public String Status;
}
