package ex2.demo_project2.demo.domain.vo;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class voOut_ReturnFreeBoardWriteResultWithStatusString {
    @JsonProperty("WRITE_DATE")
    public String WRITE_DATE;

    @JsonProperty("ID")
    public String ID;

    @JsonProperty("SUBJECT")
    public String SUBJECT;

    @JsonProperty("Status")
    public String Status;
}
