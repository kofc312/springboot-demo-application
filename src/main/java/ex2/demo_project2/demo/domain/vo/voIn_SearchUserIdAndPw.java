package ex2.demo_project2.demo.domain.vo;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class voIn_SearchUserIdAndPw {
    @JsonProperty("ID")
    public String ID;

    @JsonProperty("PW")
    public String PW;
}
