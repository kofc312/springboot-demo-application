package ex2.demo_project2.demo.domain.repo;

import ex2.demo_project2.demo.domain.entity.LogTblInfoUser;
import org.springframework.data.jpa.repository.JpaRepository;

public interface repo_LogTblInfoUser extends JpaRepository<LogTblInfoUser, String> {
}
