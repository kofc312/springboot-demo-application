package ex2.demo_project2.demo.domain.repo.queryDsl;

import com.querydsl.core.types.Path;
import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.dsl.*;
import static com.querydsl.core.types.PathMetadataFactory.forVariable;

import ex2.demo_project2.demo.domain.entity.TblInfoAuthor;

@Generated("com.querydsl.codegen.EntitySerializer")
public class q_TblInfoAuthor extends EntityPathBase<TblInfoAuthor> {
    public static final q_TblInfoAuthor qTblInfoAuthor = new q_TblInfoAuthor("qTblInfoAuthor");

    public final StringPath AU_ID = createString("AU_ID");

    public q_TblInfoAuthor(String variable) {
        super(TblInfoAuthor.class, forVariable(variable));
    }

    public q_TblInfoAuthor(Path<? extends TblInfoAuthor> path) {
        super(path.getType(), path.getMetadata());
    }

    public q_TblInfoAuthor(PathMetadata meta) {
        super(TblInfoAuthor.class, meta);
    }
}
