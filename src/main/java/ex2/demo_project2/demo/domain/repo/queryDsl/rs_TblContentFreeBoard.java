package ex2.demo_project2.demo.domain.repo.queryDsl;

import com.querydsl.core.Tuple;
import com.querydsl.jpa.impl.JPAQueryFactory;
import org.springframework.data.jpa.repository.support.QuerydslRepositorySupport;
import org.springframework.stereotype.Repository;
import java.util.List;

import ex2.demo_project2.demo.domain.entity.TblContentFreeBoard;
import static ex2.demo_project2.demo.domain.repo.queryDsl.q_TblContentFreeBoard.qTblContentFreeBoard;
import static ex2.demo_project2.demo.domain.repo.queryDsl.q_TblInfoUser.qTblInfoUser;

@Repository
public class rs_TblContentFreeBoard extends QuerydslRepositorySupport {
    private final JPAQueryFactory qFactory;

    public rs_TblContentFreeBoard(JPAQueryFactory qFactory) {
        super(TblContentFreeBoard.class);
        this.qFactory = qFactory;
    }

    public List<TblContentFreeBoard> findAll() {
        return qFactory
                .selectFrom(qTblContentFreeBoard)
                .fetch();
    }

    public List<Tuple> getAll_joinWithAuthor() {
        return qFactory
                .select(qTblContentFreeBoard.WRITE_DATE, qTblContentFreeBoard.ID)
                .from(qTblContentFreeBoard)
                .leftJoin(qTblInfoUser)
                .on(qTblInfoUser.ID.eq(qTblContentFreeBoard.ID))
                .fetch();
    }
}
