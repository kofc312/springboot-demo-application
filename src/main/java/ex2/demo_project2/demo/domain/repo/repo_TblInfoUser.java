package ex2.demo_project2.demo.domain.repo;

import ex2.demo_project2.demo.domain.entity.TblInfoUser;
import org.springframework.data.jpa.repository.JpaRepository;

public interface repo_TblInfoUser extends JpaRepository<TblInfoUser, String> {
    String getByID(String ID);
}
