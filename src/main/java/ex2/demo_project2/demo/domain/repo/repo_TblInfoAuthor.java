package ex2.demo_project2.demo.domain.repo.queryDsl;

import ex2.demo_project2.demo.domain.entity.TblInfoAuthor;
import org.springframework.data.jpa.repository.JpaRepository;

public interface repo_TblInfoAuthor extends JpaRepository<TblInfoAuthor, String> {
}
