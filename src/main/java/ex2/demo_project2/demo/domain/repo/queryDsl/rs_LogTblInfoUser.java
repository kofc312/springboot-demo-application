package ex2.demo_project2.demo.domain.repo.queryDsl;

import com.querydsl.jpa.impl.JPAQueryFactory;
import org.springframework.data.jpa.repository.support.QuerydslRepositorySupport;
import org.springframework.stereotype.Repository;
import java.util.List;

import ex2.demo_project2.demo.domain.entity.LogTblInfoUser;
import static ex2.demo_project2.demo.domain.repo.queryDsl.q_LogTblInfoUser.qLogTblInfoUser;

@Repository
public class rs_LogTblInfoUser extends QuerydslRepositorySupport {
    private final JPAQueryFactory qFactory;

    public rs_LogTblInfoUser(JPAQueryFactory qFactory) {
        super(LogTblInfoUser.class);
        this.qFactory = qFactory;
    }

    public List<LogTblInfoUser> findAll() {
        return qFactory
                .selectFrom(qLogTblInfoUser)
                .orderBy(qLogTblInfoUser.WRITE_DATE.desc())
                .fetch();
    }
}
