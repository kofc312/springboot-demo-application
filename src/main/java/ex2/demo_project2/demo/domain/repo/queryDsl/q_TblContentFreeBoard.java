package ex2.demo_project2.demo.domain.repo.queryDsl;

import com.querydsl.core.types.Path;
import com.querydsl.core.types.PathMetadata;
import com.querydsl.core.types.dsl.BooleanPath;
import com.querydsl.core.types.dsl.EntityPathBase;
import com.querydsl.core.types.dsl.StringPath;
import javax.annotation.Generated;
import static com.querydsl.core.types.PathMetadataFactory.forVariable;

import ex2.demo_project2.demo.domain.entity.TblContentFreeBoard;

@Generated("com.querydsl.codegen.EntitySerializer")
public class q_TblContentFreeBoard extends EntityPathBase<TblContentFreeBoard> {
    public static final q_TblContentFreeBoard qTblContentFreeBoard = new q_TblContentFreeBoard("qTblContentFreeBoard");

    public final StringPath WRITE_DATE = createString("WRITE_DATE");
    public final StringPath ID = createString("ID");
    public final StringPath SUBJECT = createString("SUBJECT");
    public final StringPath CONTENT = createString("CONTENT");
    public final BooleanPath IS_VALID = createBoolean("IS_VALID");

    public q_TblContentFreeBoard(String varaible) {
        super(TblContentFreeBoard.class, forVariable(varaible));
    }

    public q_TblContentFreeBoard(Path<? extends TblContentFreeBoard> path) {
        super(path.getType(), path.getMetadata());
    }

    public q_TblContentFreeBoard(PathMetadata meta) {
        super(TblContentFreeBoard.class, meta);
    }
}
