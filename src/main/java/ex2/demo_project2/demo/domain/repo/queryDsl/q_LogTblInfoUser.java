package ex2.demo_project2.demo.domain.repo.queryDsl;

import com.querydsl.core.types.Path;
import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.dsl.*;
import static com.querydsl.core.types.PathMetadataFactory.forVariable;

import ex2.demo_project2.demo.domain.entity.LogTblInfoUser;

@Generated("com.querydsl.codegen.EntitySerializer")
public class q_LogTblInfoUser extends EntityPathBase<LogTblInfoUser> {
    public static final q_LogTblInfoUser qLogTblInfoUser = new q_LogTblInfoUser("qLogTblInfoUser");

    public final StringPath WRITE_DATE = createString("WRITE_DATE");
    public final StringPath ID = createString("ID");
    public final StringPath PW = createString("PW");
    public final StringPath AU_ID = createString("AU_ID");
    public final StringPath NAME = createString("NAME");
    public final NumberPath GENDER = createNumber("GENDER", int.class);
    public final BooleanPath IS_VALID = createBoolean("IS_VALID");

    public q_LogTblInfoUser(String variable) {
        super(LogTblInfoUser.class, forVariable(variable));
    }

    public q_LogTblInfoUser(Path<? extends LogTblInfoUser> path) {
        super(path.getType(), path.getMetadata());
    }

    public q_LogTblInfoUser(PathMetadata meta) {
        super(LogTblInfoUser.class, meta);
    }
}
