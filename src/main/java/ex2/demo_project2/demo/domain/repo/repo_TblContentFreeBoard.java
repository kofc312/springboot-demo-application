package ex2.demo_project2.demo.domain.repo;

import ex2.demo_project2.demo.domain.entity.TblContentFreeBoard;
import org.springframework.data.jpa.repository.JpaRepository;

public interface repo_TblContentFreeBoard extends JpaRepository<TblContentFreeBoard, String> {
}
