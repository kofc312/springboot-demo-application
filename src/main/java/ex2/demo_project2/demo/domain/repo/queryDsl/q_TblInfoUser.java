package ex2.demo_project2.demo.domain.repo.queryDsl;

import com.querydsl.core.types.Path;
import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.dsl.*;
import static com.querydsl.core.types.PathMetadataFactory.forVariable;

import ex2.demo_project2.demo.domain.entity.TblInfoUser;

@Generated("com.querydsl.codegen.EntitySerializer")
public class q_TblInfoUser extends EntityPathBase<TblInfoUser> {
    public static final q_TblInfoUser qTblInfoUser = new q_TblInfoUser("qTblInfoUser");

    public final StringPath ID = createString("ID");
    public final StringPath PW = createString("PW");
    public final StringPath AU_ID = createString("AU_ID");
    public final StringPath NAME = createString("NAME");
    public final NumberPath GENDER = createNumber("GENDER", int.class);
    public final BooleanPath IS_VALID = createBoolean("IS_VALID");

    public q_TblInfoUser(String variable) {
        super(TblInfoUser.class, forVariable(variable));
    }

    public q_TblInfoUser(Path<? extends TblInfoUser> path) {
        super(path.getType(), path.getMetadata());
    }

    public q_TblInfoUser(PathMetadata meta) {
        super(TblInfoUser.class, meta);
    }
}
