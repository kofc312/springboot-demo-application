package ex2.demo_project2.demo.domain.repo.queryDsl;

import com.querydsl.jpa.impl.JPAQueryFactory;
import org.springframework.data.jpa.repository.support.QuerydslRepositorySupport;
import org.springframework.stereotype.Repository;
import java.util.List;

import ex2.demo_project2.demo.domain.entity.TblInfoUser;
import static ex2.demo_project2.demo.domain.repo.queryDsl.q_TblInfoUser.qTblInfoUser;

@Repository
public class rs_TblInfoUser extends QuerydslRepositorySupport {
    private final JPAQueryFactory qFactory;

    public rs_TblInfoUser(JPAQueryFactory qFactory) {
        super(TblInfoUser.class);
        this.qFactory = qFactory;
    }

    public List<TblInfoUser> findAll() {
        return qFactory
                .selectFrom(qTblInfoUser)
                .fetch();
    }

    public List<TblInfoUser> findByIdAndPw(String ID, String PW) {
        return qFactory
                .selectFrom(qTblInfoUser)
                .where(qTblInfoUser.ID.eq(ID).and(qTblInfoUser.PW.eq(PW)).and(qTblInfoUser.IS_VALID.eq(true)))
                .fetch();
    }

    public List<TblInfoUser> findByAU_ID(String AU_ID) {
        return qFactory
                .selectFrom(qTblInfoUser)
                .where(qTblInfoUser.AU_ID.eq(AU_ID).and(qTblInfoUser.IS_VALID.eq(true)))
                .fetch();
    }

    public String findById(String ID) {
        return qFactory
                .select(qTblInfoUser.ID)
                .from(qTblInfoUser)
                .where(qTblInfoUser.ID.eq(ID))
                .fetchOne();
    }
}
