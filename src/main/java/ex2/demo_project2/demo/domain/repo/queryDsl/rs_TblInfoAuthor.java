package ex2.demo_project2.demo.domain.repo.queryDsl;

import com.querydsl.jpa.impl.JPAQueryFactory;
import org.springframework.data.jpa.repository.support.QuerydslRepositorySupport;
import org.springframework.stereotype.Repository;
import java.util.List;

import ex2.demo_project2.demo.domain.entity.TblInfoAuthor;
import static ex2.demo_project2.demo.domain.repo.queryDsl.q_TblInfoAuthor.qTblInfoAuthor;

@Repository
public class rs_TblInfoAuthor extends QuerydslRepositorySupport {
    private final JPAQueryFactory qFactory;

    public rs_TblInfoAuthor(JPAQueryFactory qFactory) {
        super(TblInfoAuthor.class);
        this.qFactory = qFactory;
    }

    public List<TblInfoAuthor> findAll() {
        return qFactory
                .selectFrom(qTblInfoAuthor)
                .fetch();
    }
}
