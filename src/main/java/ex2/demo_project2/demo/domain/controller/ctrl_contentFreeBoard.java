package ex2.demo_project2.demo.domain.controller;

import ex2.demo_project2.demo.domain.service.sv_TblContentFreeBoard;
import ex2.demo_project2.demo.domain.vo.voIn_AddNewFreeBoardContent;
import ex2.demo_project2.demo.domain.vo.voOut_ReturnFreeBoardWriteResultWithStatusString;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/content/free_board")
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class ctrl_contentFreeBoard {
    @Autowired
    public sv_TblContentFreeBoard service_freeboard;

    @PostMapping("/add_new_content")
    @ApiOperation(value = "/add_new_content", notes = "자유 게시판에 새로운 글 작성")
    public voOut_ReturnFreeBoardWriteResultWithStatusString add_new_content(voIn_AddNewFreeBoardContent content) {
        return service_freeboard.add_newFreeBoardContent(content);
    }
}
