package ex2.demo_project2.demo.domain.controller;

import ex2.demo_project2.demo.domain.service.sv_LogTblInfoUser;
import ex2.demo_project2.demo.domain.vo.voOut_ReturnLogTblInfoUser;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/log")
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class ctrl_log {
    @Autowired
    public sv_LogTblInfoUser service_logUser;

    @GetMapping("/get_all_user_log")
    @ApiOperation(value = "/get_all_user_log", notes = "모든 유저 테이블의 로그 가져옴")
    public List<voOut_ReturnLogTblInfoUser> get_all_user_log() {
        return service_logUser.getAllLogUserInfo();
    }
}
