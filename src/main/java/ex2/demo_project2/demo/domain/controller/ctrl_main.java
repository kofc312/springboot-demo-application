package ex2.demo_project2.demo.domain.controller;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.view.RedirectView;

@RestController
@RequestMapping("")
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class ctrl_main {

    @GetMapping("")
    public RedirectView redirect() {
        return new RedirectView("/swagger-ui.html");
    }
}
