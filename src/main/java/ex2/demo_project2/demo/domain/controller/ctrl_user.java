package ex2.demo_project2.demo.domain.controller;


import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import java.util.List;

import ex2.demo_project2.demo.domain.vo.voIn_AddNewUser;
import ex2.demo_project2.demo.domain.vo.voIn_SearchUserIdAndPw;
import ex2.demo_project2.demo.domain.vo.voOut_ReturnUserInfoWithStatusString;
import ex2.demo_project2.demo.domain.service.sv_TblInfoUser;
import ex2.demo_project2.demo.domain.service.sv_LogTblInfoUser;
import ex2.demo_project2.demo.domain.vo.voOut_ReturnLogTblInfoUser;

@RestController
@RequestMapping("/user")
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class ctrl_user {

    @Autowired
    public sv_TblInfoUser service_user;

    @Autowired
    public sv_LogTblInfoUser service_logUser;

    @GetMapping("/")
    public String helloWorld() {
        return "hello";
    }

    @GetMapping(value = "/all")
    @ApiOperation(value = "/all", notes = "모든 사용자 정보 반환")
    public List<voOut_ReturnUserInfoWithStatusString> get_all_user() {
        return service_user.getAllUserInfo();
    }

    @PostMapping("/search")
    @ApiOperation(value = "/search", notes = "아이디, 패스워드로 검색하여 해당하는 사용자 정보 반환")
    public voOut_ReturnUserInfoWithStatusString search_user(@RequestBody voIn_SearchUserIdAndPw user) {
        String ID = user.ID;
        String PW = user.PW;

        return service_user.searchUserIdAndPw(ID, PW);
    }

    @PostMapping("/add_new_user")
    @ApiOperation(value = "/add_new_user", notes = "아이디, 패스워드, 이름, 성별을 입력하여 사용자 정보 추가")
    public String add_new_user(@RequestBody voIn_AddNewUser user) {
        return service_user.addNewUser(user);
    }

    @PostMapping("/update_user_info")
    @ApiOperation(value = "/update_user_info", notes = "선택한 회원 정보 수정")
    public String update_user_info(@RequestBody voIn_AddNewUser user) {
        return service_user.updateUserInfo(user);
    }

    /*
    @PostMapping("/insert_logUser")
    @ApiOperation(value = "/insert_logUser", notes = "test")
    public String insert_logUser(@RequestBody voIn_AddNewUser user) {
        return service_logUser.writeLogAtTblUserInfo(user);
    }
    */
}
